public class Article {
    private String number;
    private String name;
    private String manufacturer;

    public Article(String number, String name, String manufacturer) {
        this.number = number;
        this.name = name;
        this.manufacturer = manufacturer;
    }

    public String getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getManufacturer() {
        return manufacturer;

    }
}
